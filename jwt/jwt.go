package jwt

import (
	"database/sql"
	_ "embed"
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	_ "github.com/lib/pq"
	"log"
	"net/http"
	"os"
	"strings"
	"twittor/gogrest"
	"twittor/helpers"
	"twittor/responses"
)

const (
	pubKeyPath = "public_key" // openssl rsa -in app.rsa -pubout > app.rsa.pub
)

type LaravelClaims struct {
	jwt.RegisteredClaims
}

// ValidTokenWithKeys verificar un token firmado
func ValidTokenWithKeys(tokenString string, url string) (string, error) {
	if tokenString == "" {
		return "", errors.New(fmt.Sprintf("el token es requerido, %v", url))
	}
	// validar la firma del token
	parts := strings.Split(tokenString, ".")
	if len(parts) < 3 {
		return "", errors.New("El token no es valido")
	}

	// leer la publickey
	keyData, err := os.ReadFile(pubKeyPath)
	if err != nil {
		return "", err
	}
	// decodificar key
	key, err := jwt.ParseRSAPublicKeyFromPEM(keyData)
	if err != nil {
		return "", err
	}

	err = jwt.SigningMethodRS256.Verify(strings.Join(parts[0:2], "."), parts[2], key)
	if err != nil {
		return "", err
	}

	// parceo del token con la public key
	token, err := jwt.ParseWithClaims(
		tokenString,
		&LaravelClaims{},
		func(token *jwt.Token) (interface{}, error) {
			// en esta función retornamos la public key para que se pase internamente al algoritmo y lo pueda validar
			return key, nil
		},
	)
	// validar token
	if err != nil {
		if errors.Is(err, jwt.ErrTokenMalformed) {
			fmt.Println("That's not even a token")
		} else if errors.Is(err, jwt.ErrTokenExpired) || errors.Is(err, jwt.ErrTokenNotValidYet) {
			// Token is either expired or not active yet
			fmt.Println("Timing is everything")
		} else {
			fmt.Println("Couldn't handle this token:", err)
			log.Fatalf("Failed to parse the JWT.\nError: %s", err.Error())
		}
		return "", err
	}
	claims, _ := token.Claims.(*LaravelClaims)

	isRevoked, err := isRevoke(claims.ID)
	if err != nil {
		return "", err
	}

	if token.Valid && isRevoked {
		return "", errors.New("El token no es valido")
	}

	return claims.Subject, nil
}

// GetHeaderToken retornar el token del usuario sin formatos
func GetHeaderToken(token string) string {
	// quitare l Bearer
	token = strings.Replace(token, "Bearer", "", 1)
	// eliminar espacios
	token = strings.Trim(token, " ")
	return token
}

// CheckUserToken HTTP middleware to check user access_token
func CheckUserToken(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//var userId string
		var err error
		ignoreRoutes := []string{
			"/",
			"/swagger",
		}
		isPresent := helpers.Contains(ignoreRoutes, r.RequestURI)
		isSwagger := strings.Contains(r.RequestURI, "/swagger/")
		isPing := strings.Contains(r.RequestURI, "/ping/")
		isAssets := helpers.IsAssets(r.RequestURI)

		if !isPresent && !isSwagger && !isPing && !isAssets {
			//logs.HandleError(errors.New(fmt.Sprintf("Req: Host-> %s  @ URL.Path-> %s  @ Method-> %s  @ UserAgent-> %s\n", r.Host, r.URL.Path, r.Method, r.UserAgent())))

			//var token string
			//token = GetHeaderToken(r.Header.Get("Authorization"))

			//userId, err = ValidTokenWithKeys(token, r.RequestURI)

			if err != nil {
				// logs.HandleError(err)
				responses.ResponseUnauthorized(w, r)
				return
			}
		}
		// user, _ := getUserAuth(userId)
		// ctx = context.WithValue(r.Context(), "user", user)

		next.ServeHTTP(w, r)
	})
}

func isRevoke(id string) (bool, error) {
	db, err := gogrest.ConnectPgsql()

	if err != nil {
		return false, err
	}

	defer func(db *sql.DB) {
		err := db.Close()
		if err != nil {
			return
		}
	}(db)

	var revoked bool

	err = db.QueryRow("SELECT revoked FROM oauth_access_tokens WHERE id = $1;", id).Scan(&revoked)
	if err != nil {
		return false, err
	}

	return revoked, nil
}
