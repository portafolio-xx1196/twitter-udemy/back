package connect

import (
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
	"twittor/models"
)

var userCollection = GetCollection(DB, "users")

// Register create new user in mongo db
// return user data
func Register(user models.User) (*models.User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//validate the user not exist
	var selectFields = bson.M{}
	var search = bson.D{
		{"email", user.Email},
	}
	var userSearch models.User
	var opts = options.FindOne().SetProjection(selectFields)
	err := userCollection.FindOne(ctx, search, opts).Decode(&userSearch) //verify if user by email exits

	if err != nil { //insert if not exists
		user.NormalizeUser()

		_, errInsert := userCollection.InsertOne(ctx, user)
		if errInsert != nil {
			return nil, errInsert
		}
	} else {
		return nil, errors.New("user exists")
	}

	return &user, nil
}
