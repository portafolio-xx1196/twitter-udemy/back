package connect

/*
import (
	"context"
	"fmt"
	"github.com/strikesecurity/strikememongo"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"os"
	"strings"
	"testing"
	"time"
	"twittor/server/logs"
)

// ----------------------------------------
//
//	AUXILIARY VARIABLES AND CONSTANTS
//
// ----------------------------------------
const (
	// collection constants
	categoriesCollectionTestName = "categories"
	videosCollectionNameTest     = "videos"
)

var (
	// collections variables
	categoriesCollectionTest *mongo.Collection
	videosCollectionTest     *mongo.Collection

	databaseName = ""
	mongoURI     = ""
	database     *mongo.Database
)

// ----------------------------
// 		TEST MAIN FUNCTION
// ----------------------------

func TestMain(m *testing.M) {
	os.Exit(0)
	mongoServer, err := strikememongo.Start("4.0.5")
	if err != nil {
		log.Fatal(err)
	}
	mongoURI = mongoServer.URIWithRandomDB()
	splitedDatabaseName := strings.Split(mongoURI, "/")
	databaseName = splitedDatabaseName[len(splitedDatabaseName)-1]

	defer mongoServer.Stop()

	exitVal := m.Run()

	os.Exit(exitVal)
}

func TestMongoURI(t *testing.T) {
	if mongoURI == "" {
		t.Fatal("MONGO URI ")
	}
}

// ----------------------------
//
//	SET UP FUNCTION
//
// ----------------------------
func TestSetup(t *testing.T) {
	startApplication(t)
	createCollections()
	cleanup(t)
}

func initDB() (client *mongo.Client, ctx context.Context) {
	uri := fmt.Sprintf("%s%s", mongoURI, "?retryWrites=false")
	configuration.LoadEnv(".env.ci")

	if uri == "" {
		uri = CreateString()
	}

	applyUrl := options.Client().ApplyURI(uri)
	var err error
	client, err = mongo.NewClient(applyUrl)

	if err != nil {
		logs.HandleError(err)
	}
	ctx, _ = context.WithTimeout(context.Background(), 60*time.Minute)
	//ctx = context.Background()
	err = client.Connect(ctx)
	if err != nil {
		logs.HandleError(err)
	}
	return client, ctx
}

// startApplication initializes the engine and the necessary components for the (test) service to work
func startApplication(t *testing.T) {
	// Initialize Database (memongodb)
	dbClient, ctx := initDB()

	err := dbClient.Ping(ctx, readpref.Primary())
	if err != nil {
		t.Fatal("error connecting to database", err)
	}

	database = dbClient.Database(databaseName)
}

// createCollections cretaes the necessary collections to be used during tests
func createCollections() {
	err := database.CreateCollection(context.Background(), categoriesCollectionTestName)
	if err != nil {
		fmt.Printf("error creating collection: %s", err.Error())
	}

	err = database.CreateCollection(context.Background(), videosCollectionNameTest)
	if err != nil {
		fmt.Printf("error creating collection: %s", err.Error())
	}

	categoriesCollectionTest = database.Collection(categoriesCollectionTestName)
	videosCollectionTest = database.Collection(videosCollectionNameTest)
}

// ----------------------------
//
//	TEAR DOWN FUNCTION
//
// ----------------------------
func cleanup(t *testing.T) {
	_, err := categoriesCollectionTest.DeleteMany(context.Background(), bson.M{})
	if err != nil {
		t.Fatal(fmt.Sprintf("error deleting collection: %s", err.Error()))
	}
	_, err = videosCollectionTest.DeleteMany(context.Background(), bson.M{})
	if err != nil {
		t.Fatal(fmt.Sprintf("error deleting collection: %s", err.Error()))
	}
}
*/
