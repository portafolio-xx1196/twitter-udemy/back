package connect

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
	"twittor/config"
)

var configuration config.Config
var DB = ConnectDB()

// ConnectDB InitializeDatabase inicia  la conexión con mongo db
func ConnectDB() *mongo.Client {
	configuration.LoadEnv(".env")

	client, err := mongo.NewClient(options.Client().ApplyURI(CreateString()))
	if err != nil {
		log.Fatal(err)
	}

	ctx, cancelFunc := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancelFunc()

	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connecting to MongoDB")
	//ping the database
	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connected to MongoDB")
	return client
}

// GetCollection getting database collections
func GetCollection(client *mongo.Client, collectionName string) *mongo.Collection {
	collection := client.Database(configuration.Database).Collection(collectionName)
	return collection
}

// CreateString crear el string de conexion con mongo
// return configuration string
func CreateString() string {
	return fmt.Sprintf("%s://%s:%s@%s/%s",
		configuration.Host,
		configuration.Username,
		configuration.Password,
		configuration.Cluster,
		configuration.Database,
	)
}
