package helpers

import (
	"github.com/joho/godotenv"
	"golang.org/x/crypto/bcrypt"
	"math"
	"os"
	"strconv"
	"strings"
	"time"
)

// LoadEnvFile load .env file
func LoadEnvFile(file string) error {
	err := godotenv.Load(file)
	return err
}

// Contains check if array contain string
func Contains(sl []string, name string) bool {
	for _, value := range sl {
		if value == name {
			return true
		}
	}
	return false
}

// ContainsString check if array contain string
func ContainsString(sl []string, name string) bool {
	for _, value := range sl {
		if strings.Contains(name, value) {
			return true
		}
	}
	return false
}

// ContainsInt64 check if array contain int64
func ContainsInt64(sl []int64, search int64) bool {
	for _, value := range sl {
		if value == search {
			return true
		}
	}
	return false
}

// IsAssets validar si el string contiene algun formato de archivo estatico
func IsAssets(name string) bool {
	return ContainsString(
		[]string{
			".ico",
			".js",
			".css",
			".png",
			".jpg",
			".jpeg",
			".gif",
			".html",
			".env",
		},
		name,
	)
}

// BoolPointer retornar un booleano con puntero
func BoolPointer(b bool) *bool {
	return &b
}

func NowPointer(time time.Time) *time.Time {
	return &time
}

// CheckIfIsBool check if data is true
func CheckIfIsBool(s string, defaultValue bool) bool {
	val, err := strconv.ParseBool(s)

	if err != nil {
		return defaultValue
	}
	return val
}

// CheckFolderAndCreate create folder
func CheckFolderAndCreate(folderName string) error {
	if _, err := os.Stat(folderName); os.IsNotExist(err) {
		return os.MkdirAll(folderName, 0777)
	}
	return nil
}

// PathsPerCPU devuelve el número de caminos por CPU, aproximando hacia arriba
func PathsPerCPU(numPaths, numCPUs int) int {
	return int(math.Ceil(float64(numPaths) / float64(numCPUs)))
}

// EncryptString encrypt a text
func EncryptString(text string) (string, error) {
	cost := 8 //2**cost, more cost more time

	bytes, err := bcrypt.GenerateFromPassword([]byte(text), cost)

	return string(bytes), err
}
