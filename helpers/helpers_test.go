package helpers

import (
	"os"
	"testing"
)

func TestLoadEnvFile(t *testing.T) {
	// Crear un archivo de entorno temporal
	file, err := os.Create("test.env")
	if err != nil {
		t.Errorf("Error al crear archivo temporal: %s", err)
	}
	file.WriteString("TEST_VAR=test_value")
	file.Close()
	defer os.Remove("test.env")

	// Llamar a la función con el archivo temporal
	err = LoadEnvFile("test.env")
	if err != nil {
		t.Errorf("Error al cargar archivo: %s", err)
	}
	// Comprobar que la variable de entorno ha sido cargada
	testValue := os.Getenv("TEST_VAR")
	if testValue != "test_value" {
		t.Errorf("Valor de la variable de entorno no es correcto: %s", testValue)
	}
	// Llamar a la función con un archivo que no existe
	err = LoadEnvFile("non_existent_file.env")
	if err == nil {
		t.Errorf("Debería haber ocurrido un error al cargar el archivo no existente")
	}
}

func TestCheckFolderAndCreate(t *testing.T) {
	//Crear un nombre temporal para el directorio
	folderName := "temp_folder"
	defer os.RemoveAll(folderName)

	//Llamar a la función con el directorio temporal
	err := CheckFolderAndCreate(folderName)
	if err != nil {
		t.Errorf("Error al crear directorio: %s", err)
	}

	//Comprobar que el directorio ha sido creado
	if _, err := os.Stat(folderName); os.IsNotExist(err) {
		t.Errorf("Directorio no fue creado")
	}

	//Llamar a la función de nuevo con el mismo directorio temporal para validar que no da error al existir el directorio
	err = CheckFolderAndCreate(folderName)
	if err != nil {
		t.Errorf("Error al crear directorio: %s", err)
	}
}
