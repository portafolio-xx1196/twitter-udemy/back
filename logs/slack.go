package logs

import (
	"github.com/ashwanthkumar/slack-go-webhook"
	"os"
	"twittor/helpers"
)

func SendToSlack(err error) {
	helpers.LoadEnvFile(".env")
	webhookUrl := os.Getenv("SLACK_WEBHOOK")
	payload := slack.Payload{
		Text:      err.Error(),
		Username:  "GoRobot",
		Channel:   os.Getenv("SLACK_CHANNEL"),
		IconEmoji: ":ahhh:",
	}
	slack.Send(webhookUrl, "", payload)
}
