package logs

import (
	"fmt"
)

func HandleError(err error) {
	fmt.Println(err)
	// sentry.CaptureException(err)
	SendToSlack(err)
}
