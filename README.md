# back

# Ejecución
Para ejecutar esta imagen, debes utilizar el siguiente comando:

```bash
docker run -d -v "$(pwd)/.env:/.env" -p <external-port>:8001  xx1196/twittor-back:latest
```
Este comando utiliza la opción -v para montar el archivo .env en el contenedor. Asegúrate de crear un archivo .env en tu directorio actual, basado en el archivo .env.example proporcionado. También puedes modificar el comando para apuntar al directorio correcto que contiene tu archivo .env.

# Licencia
Este proyecto está disponible bajo la Licencia MIT. Consulta el archivo LICENSE para obtener más información.