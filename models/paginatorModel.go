package models

import (
	"encoding/json"
	"math"
	"net/http"
	"strconv"
)

type PaginationRequest struct {
	Page int64 `json:"page,omitempty"`
}

func UnmarshalPaginationRequest(data []byte) (PaginationRequest, error) {
	var r PaginationRequest
	err := json.Unmarshal(data, &r)
	if r.Page <= 0 {
		r.Page = 1
	}
	return r, err
}

func (p *PaginationRequest) GetPage(r *http.Request) {
	page, errorPage := strconv.Atoi(r.URL.Query().Get("page"))

	if errorPage != nil || page <= 0 {
		page = 1
	}

	p.Page = int64(page)
}

type PaginationMetadata struct {
	Total       int64 `json:"total,omitempty"`
	CurrentPage int64 `json:"current_page,omitempty"`
	LastPage    int64 `json:"last_page,omitempty"`
	PerPage     int64 `json:"per_page,omitempty"`
}

func UnmarshalPaginationMetadata(data []byte) (PaginationMetadata, error) {
	var r PaginationMetadata
	err := json.Unmarshal(data, &r)
	return r, err
}
func (p *PaginationMetadata) GetLimit() int64 {
	return (p.CurrentPage - 1) * p.PerPage
}
func (p *PaginationMetadata) SetLastPage() {
	var floatTotal = float64(p.Total)
	var floatPerPage = float64(p.PerPage)
	var result = math.Ceil(floatTotal / floatPerPage)
	p.LastPage = int64(result)
}
