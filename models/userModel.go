// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    UserData, err := UnmarshalUserData(bytes)
//    bytes, err = UserData.Marshal()

package models

import (
	"encoding/json"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
	"twittor/helpers"
)

/*func UnmarshalUserData(data []byte) (UserData, error) {
	var r UserData
	err := json.Unmarshal(data, &r)
	return r, err
}*/

func (userData *UserData) MarshalUserData() ([]byte, error) {
	return json.Marshal(userData)
}

func UnmarshalUser(data []byte) (User, error) {
	var r User
	err := json.Unmarshal(data, &r)
	return r, err
}

func (userData *UserData) MarshalUser() ([]byte, error) {
	return json.Marshal(userData)
}

type UserData struct {
	Data User `json:"data,omitempty"`
}

type User struct {
	ID        primitive.ObjectID `bson:"_id,omitempty" json:"_id"`
	Name      string             `bson:"name" json:"name,omitempty"  validate:"required"`
	Lastname  string             `bson:"lastname" json:"lastname,omitempty"`
	Email     string             `bson:"email" json:"email,omitempty" validate:"required,email"`
	Password  string             `bson:"password" json:"password,omitempty" validate:"required,min=6"`
	Avatar    string             `bson:"avatar" json:"avatar,omitempty"`
	Banner    string             `bson:"banner" json:"banner,omitempty"`
	Biography string             `bson:"biography" json:"biography,omitempty"`
	Birthdate time.Time          `bson:"birthdate" json:"birthdate,omitempty"`
}

// NormalizeUserData Funciones para creaciones
func (userData *UserData) NormalizeUserData() {
	userData.Data.ID = primitive.NewObjectID()
}

// NormalizeUser Funciones para creaciones
func (user *User) NormalizeUser() {
	user.ID = primitive.NewObjectID()
	pass, err := helpers.EncryptString(user.Password)

	if err != nil {
		pass = "hola-mundo"
	}

	user.Password = pass
}
