package models

import (
	"testing"
)

var paginationData = PaginationMetadata{
	PerPage:     10,
	CurrentPage: 1,
	Total:       12,
}

func TestLastPage(t *testing.T) {
	paginationData.SetLastPage()

	if paginationData.LastPage != 2 {
		t.Fatal("Error la cantidad de paginas debe ser 2")
	}
}

func TestLimit(t *testing.T) {
	var limit = paginationData.GetLimit()

	if limit != 0 {
		t.Fatal("Error el limite es 0")
	}
}

func TestUnmarshalPaginationMetadata(t *testing.T) {
	Data := []byte(`{
        "total": 1,  
        "current_page": 1,
        "last_page": 1,
        "per_page": 1
    }`)

	paginationMetadata, err := UnmarshalPaginationMetadata(Data)

	if err != nil {
		t.Fatal("Error UnmarshalPaginationMetadata")
	}
	if paginationMetadata.Total != 1 {
		t.Fatal("Error UnmarshalPaginationMetadata Total")
	}
	if paginationMetadata.CurrentPage != 1 {
		t.Fatal("Error UnmarshalPaginationMetadata CurrentPage")
	}
	if paginationMetadata.LastPage != 1 {
		t.Fatal("Error UnmarshalPaginationMetadata LastPage")
	}
	if paginationMetadata.PerPage != 1 {
		t.Fatal("Error UnmarshalPaginationMetadata PerPage")
	}
}

func TestUnmarshalPaginationRequest(t *testing.T) {
	Data := []byte(`{
        "page": 1
    }`)

	paginationRequest, err := UnmarshalPaginationRequest(Data)

	if err != nil {
		t.Fatal("Error UnmarshalPaginationRequest")
	}
	if paginationRequest.Page != 1 {
		t.Fatal("Error UnmarshalPaginationRequest Page")
	}
}

func TestUnmarshalPaginationDefaultRequest(t *testing.T) {
	Data := []byte(`{
        "page": 0
    }`)

	paginationRequest, err := UnmarshalPaginationRequest(Data)

	if err != nil {
		t.Fatal("Error UnmarshalPaginationRequest")
	}
	if paginationRequest.Page != 1 {
		t.Fatal("Error UnmarshalPaginationRequest Page")
	}
}
