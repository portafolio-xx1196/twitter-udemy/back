package config

import (
	"os"
	"strconv"
	"twittor/helpers"
)

type Config struct {
	Username string
	Password string
	Database string
	Cluster  string
	Host     string
	PerPage  int64
}
type ConfigPgsql struct {
	Username string
	Password string
	Database string
	Port     string
	Host     string
}

func (c *Config) LoadEnv(file string) {
	_ = helpers.LoadEnvFile(file)
	c.Username = os.Getenv("DB_USERNAME")
	c.Password = os.Getenv("DB_PASSWORD")
	c.Database = os.Getenv("DB_DATABASE")
	c.Cluster = os.Getenv("DB_CLUSTER")
	c.Host = os.Getenv("DB_HOST")
}

func (c *ConfigPgsql) LoadEnvPgsql(file string) {
	_ = helpers.LoadEnvFile(file)
	c.Username = os.Getenv("DB_PGSQL_USER")
	c.Password = os.Getenv("DB_PGSQL_PASSWORD")
	c.Database = os.Getenv("DB_PGSQL_NAME")
	c.Port = os.Getenv("DB_PGSQL_PORT")
	c.Host = os.Getenv("DB_PGSQL_HOST")
}

func GetPerPage() int64 {
	perpage, err := strconv.Atoi(os.Getenv("PAGINATION_PER_PAGE"))
	if err != nil {
		return 20
	}
	return int64(perpage)
}
