package config

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
	"twittor/helpers"
)

func TestGetPerPageDefault(t *testing.T) {
	perPage := GetPerPage()
	assert.True(t, perPage == 20)
}

func TestConfig_LoadEnv(t *testing.T) {
	if err := helpers.LoadEnvFile("../.env.ci"); err != nil {
		t.Error("Error loading .env file")
	}
}

func TestGetPerPageEnv(t *testing.T) {
	perPage := GetPerPage()
	assert.True(t, perPage == 10)
}

func TestConfigData(t *testing.T) {
	var configuration Config
	configuration.LoadEnv("../.env.ci")
	fmt.Println(configuration.PerPage)
	// Output: 10
}

func TestConfigDataPgsql(t *testing.T) {
	var configuration ConfigPgsql
	configuration.LoadEnvPgsql("../.env.ci")
	fmt.Println(configuration.Host)
	// Output: host
}
