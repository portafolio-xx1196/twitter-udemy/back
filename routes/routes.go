package routes

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/render"
	httpSwagger "github.com/swaggo/http-swagger"
	"os"
	"strings"
	"time"
	"twittor/controllers"
	"twittor/docs"
	"twittor/jwt"
)

// @version         1.0
// @termsOfService  http://swagger.io/terms/

// @contact.name   API Support
// @contact.url    http://www.swagger.io/support
// @contact.email  support@swagger.io

// @license.name  Apache 2.0
// @license.url   http://www.apache.org/licenses/LICENSE-2.0.html

//	@securityDefinitions.basic	BasicAuth

//	@securityDefinitions.apikey	ApiKeyAuth
//	@in							header
//	@name						Authorization
//	@description				Description for what is this security definition being used
//
// Routes
func Routes() *chi.Mux {
	// programmatically set swagger info
	docs.SwaggerInfo.Title = "Mongo API documentation"
	docs.SwaggerInfo.Description = "This is a evolve api documentation."
	docs.SwaggerInfo.Host = os.Getenv("APP_HOST")
	docs.SwaggerInfo.BasePath = "/"
	docs.SwaggerInfo.Schemes = []string{"http", "https"}

	// crear enrutador
	r := chi.NewRouter()
	r.Use(middleware.Timeout(60 * time.Hour)) // 5 min
	r.Use(middleware.NoCache)                 // deshabilitar cache
	r.Use(middleware.StripSlashes)            // habilitar que se ignore si la URL tiene al final un "/"
	if os.Getenv("APP_ENV") == "develop" {
		r.Use(middleware.Logger) // registros de solicitudes
	}

	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.Use(middleware.AllowContentType([]string{
		"multipart/form-data",
		"application/x-www-form-urlencoded",
		"application/json",
		"text/javascript",
	}...)) // application/x-www-form-urlencoded

	// habilitar las conexiones desde cualquier sitio

	r.Use(cors.Handler(cors.Options{
		// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: strings.Split(os.Getenv("ACCEPTED_DOMAINS"), ","),
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods: []string{"GET", "POST", "PUT", "HEAD", "OPTIONS"},
		AllowedHeaders: []string{"X-Requested-With", "Content-Type", "Authorization"},
		//ExposedHeaders:   []string{"Link"},
		//AllowCredentials: false,
		//MaxAge:           300, // Maximum value not ignored by any of major browsers
	}))

	r.Use(jwt.CheckUserToken) // Validación del token del usuario en la petición

	r.Get("/ping", controllers.Ping)

	r.Route("/auth", func(r chi.Router) {
		r.Post("/register", controllers.Register)
	})
	if os.Getenv("APP_ENV") == "develop" {
		r.Mount("/swagger", httpSwagger.WrapHandler)
	}

	return r
}
