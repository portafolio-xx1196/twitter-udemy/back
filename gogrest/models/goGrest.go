package models

import (
	"os"
	"twittor/helpers"
)

type ConfigPgsql struct {
	Username string
	Password string
	Database string
	Port     string
	Host     string
}

func (c *ConfigPgsql) LoadEnvPgsql(file string) {
	_ = helpers.LoadEnvFile(file)
	c.Username = os.Getenv("DB_PGSQL_USER")
	c.Password = os.Getenv("DB_PGSQL_PASSWORD")
	c.Database = os.Getenv("DB_PGSQL_NAME")
	c.Port = os.Getenv("DB_PGSQL_PORT")
	c.Host = os.Getenv("DB_PGSQL_HOST")
}
