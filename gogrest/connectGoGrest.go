package gogrest

import (
	"database/sql"
	"fmt"
	"twittor/gogrest/models"
)

var configuration models.ConfigPgsql

func CreateString() string {
	return fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		configuration.Host,
		configuration.Port,
		configuration.Username,
		configuration.Password,
		configuration.Database,
	)
}

func ConnectPgsql() (*sql.DB, error) {
	configuration.LoadEnvPgsql(".env")
	psqlInfo := CreateString()

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		return nil, err
	}

	err = db.Ping()

	if err != nil {
		panic(err)
	}

	return db, nil
}
