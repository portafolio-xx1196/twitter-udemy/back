package server

import (
	"context"
	"testing"
	"time"
)

func TestStartServer(t *testing.T) {
	srv := NewServer(":45566", nil)
	go func() {
		time.Sleep(1 * time.Second)
		_ = srv.Shutdown(context.Background())
	}()
	err := srv.Start()
	if err != nil {
		t.Error("unexpected error:", err)
	}
}
