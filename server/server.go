package server

import (
	"fmt"
	"github.com/go-chi/chi"
	"net/http"
	"twittor/logs"
	"twittor/routes"
)

func StartServer() {

	var router = routes.Routes()
	server := NewServer(":8001", router)
	err := server.Start()
	if err != nil {
		logs.HandleError(err)
		return
	}
}

type Server struct {
	*http.Server
}

func NewServer(adr string, handler *chi.Mux) Server {
	return Server{
		&http.Server{
			Addr:    adr,
			Handler: handler,
		},
	}
}

func (s Server) Start() error {
	fmt.Println("Server start")
	if err := s.ListenAndServe(); err != http.ErrServerClosed {
		return err
	}
	return nil
}
