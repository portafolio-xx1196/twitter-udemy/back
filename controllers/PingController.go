package controllers

import (
	"github.com/go-chi/render"
	"net/http"
	"twittor/responses"
)

func Ping(w http.ResponseWriter, r *http.Request) {
	response := responses.ResponseOk(nil, "pong")
	render.JSON(w, r, response)
}
