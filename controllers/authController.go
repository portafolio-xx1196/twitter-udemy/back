package controllers

import (
	"github.com/go-chi/render"
	"github.com/go-playground/validator/v10"
	"io"
	"net/http"
	"twittor/connect"
	"twittor/logs"
	"twittor/models"
	"twittor/responses"
)

// Register get only categories data
// @Summary Datos de las categorias
// @Description Registra un usuario nuevo
// @Tags Categories
// @Accept       json
// @Produce      json
//
//	@Param        user  body      models.User true  "Add user"
//
// @Success 	 200  {array}   models.User
// @Failure      400  {object}  responses.Response
// @Failure      401  {object}  responses.Response
// @Failure      404  {object}  responses.Response
// @Failure      422  {object}  responses.Response
// @Failure      500  {object}  responses.Response
// @Router /auth/register [post]
func Register(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := io.ReadAll(r.Body)
	user, err := models.UnmarshalUser(reqBody)

	if err != nil {
		if err != nil {
			logs.HandleError(err)
			responses.ResponseError(w, r, err.Error(), "Error in received data ")
			return
		}
	}

	err = validate.Struct(user)
	if err != nil {
		// translate all error at once
		errs := err.(validator.ValidationErrors)
		responses.ResponseError(w, r, errs.Translate(trans), "Upps, no hemos podido procesar tu solicitud")
		return
	}

	var response responses.Response

	userCreate, err := connect.Register(user)
	if err != nil {
		logs.HandleError(err)
		responses.ResponseError(w, r, userCreate, err.Error())
		return
	}
	response = responses.ResponseOk(userCreate, "")

	render.JSON(w, r, response)
}
