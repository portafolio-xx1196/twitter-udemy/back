package controllers

import (
	"github.com/go-playground/locales/es"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	es_translations "github.com/go-playground/validator/v10/translations/es"
	"twittor/logs"
)

var uni *ut.UniversalTranslator
var validate *validator.Validate
var trans ut.Translator

func init() {
	esTranslator := es.New()
	uni = ut.New(esTranslator, esTranslator)
	trans, _ = uni.GetTranslator("es")
	validate = validator.New()
	err := es_translations.RegisterDefaultTranslations(validate, trans)
	if err != nil {
		logs.HandleError(err)
	}
}
